% ADP learning
function U = ADPlearn(numberOfStates,Ra,modifiedInP,whereCanIGo)
    P = spalloc(numberOfStates,numberOfStates,(numberOfStates - 4099)*49);
    for i = 44100 : numberOfStates
        for j = 1 : 49
            index = int32(whereCanIGo(i,j));
            if index == 0
                index = 1;
            end
            if index ~= -1
                P(i,index) = modifiedInP(i,j);
            end
        end
    end
    U = getUtility(Ra,P,numberOfStates);
end
function U = getUtility(R,P,numberOfStates)             % calculate new U vector
    epsilon = 0.01;                                     % discount factor
    I = speye(numberOfStates,numberOfStates);           % identity matrix
    b = P * -1 * R';
    A = epsilon * P - I;
    U = A\b;
end


    
        
        
        
        
        
        
        
        
