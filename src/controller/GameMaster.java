package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;














import java.util.Map.Entry;
import java.util.TreeMap;

import javax.sound.sampled.ReverbType;

import matlabcontrol.MatlabConnectionException;
import matlabcontrol.MatlabInvocationException;
import matlabcontrol.MatlabProxy;
import matlabcontrol.MatlabProxyFactory;



import matlabcontrol.extensions.MatlabNumericArray;
import matlabcontrol.extensions.MatlabTypeConverter;
import agents.Agent;
import agents.BestExpectedValueAgent;
import agents.MinimaxAgent;
import agents.HumanAgent;
import agents.RandomAgent;
import world.Card;
import world.DeckFactory;
import world.Decks;
import world.Game;
import world.Player;

public class GameMaster {			// the main class that controls the Game
	private Game game;
	private double[][] whereCanIGo;
	private int WinsOfA;
	private int WinsOfB;
	public static void main(String[] args) {
		GameMaster gameMaster = new GameMaster();
//		gameMaster.createRandomGame(Decks.ARMOR,Decks.DPM);
//		for (int i = 0; i < 1000; i++) {
//			gameMaster.playGame();
//		}
//		System.out.println("A won: " + gameMaster.WinsOfA + " times");
//		System.out.println("B won: " + gameMaster.WinsOfB + " times");
//		gameMaster.WinsOfA = 0;
//		gameMaster.WinsOfB = 0;
//		gameMaster.createRandomVsAi(Decks.ARMOR,Decks.DPM);
//		for (int i = 0; i < 1000; i++) {
//			gameMaster.playGame();
//		}
//		System.out.println("A won: " + gameMaster.WinsOfA + " times");
//		System.out.println("B won: " + gameMaster.WinsOfB + " times");
//		gameMaster.WinsOfA = 0;
//		gameMaster.WinsOfB = 0;
//		gameMaster.createHumanVsAi(Decks.ARMOR,Decks.DPM);
//		for (int i = 0; i < 10; i++) {
//			gameMaster.playGame();
//		}
//		System.out.println("A won: " + gameMaster.WinsOfA + " times");
//		System.out.println("B won: " + gameMaster.WinsOfB + " times");
//		gameMaster.WinsOfA = 0;
//		gameMaster.WinsOfB = 0;
//		gameMaster.createAIVsAi(Decks.ARMOR,Decks.DPM);
//		for (int i = 0; i < 1000; i++) {
//			gameMaster.playGame();
//		}
//		System.out.println("A won: " + gameMaster.WinsOfA + " times");
//		System.out.println("B won: " + gameMaster.WinsOfB + " times");
//		gameMaster.WinsOfA = 0;
//		gameMaster.WinsOfB = 0;
		gameMaster.balance();
	}
	public void createRandomGame(Decks deckA, Decks deckB)
	{
		Agent A = new RandomAgent(DeckFactory.getDeck(deckA));
		Agent B = new RandomAgent(DeckFactory.getDeck(deckB));
		game = new Game(A, B);
		List<Card> deckOfA = DeckFactory.getDeck(deckA);
		List<Card> deckOfB = DeckFactory.getDeck(deckB);
		game.setRewardVectors(deckOfA, deckOfB);
		System.out.println("Game Starting With Two Random Agents");
		System.out.println("====================================");
	}
	public void createRandomVsAi(Decks deckA, Decks deckB)
	{
		game = new Game();
		game.generateJumpTables();
		List<Card> deckOfA = DeckFactory.getDeck(deckA);
		List<Card> deckOfB = DeckFactory.getDeck(deckB);
		game.setRewardVectors(deckOfA, deckOfB);
		double [] Rb = this.game.getRewardB();
		MatlabProxyFactory factory = new MatlabProxyFactory();
	    MatlabProxy proxy = null;
		try {
			proxy = factory.getProxy();
		} catch (MatlabConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		double[] U = learn(Rb,proxy);
		Agent B = new BestExpectedValueAgent(DeckFactory.getDeck(deckB), U, game.getReverseJumpTable());
		Agent A = new RandomAgent(DeckFactory.getDeck(deckA));
		game.SetAgents(A, B);
		System.out.println("Game Starting With A as AI and B as Random Agents");
		System.out.println("====================================");
	}
	public void createAIVsAi(Decks deckA, Decks deckB)
	{
		game = new Game();
		game.generateJumpTables();
		List<Card> deckOfA = DeckFactory.getDeck(deckA);
		List<Card> deckOfB = DeckFactory.getDeck(deckB);
		game.setRewardVectors(deckOfA, deckOfB);
		double [] Ra = this.game.getRewardA();
		double [] Rb = this.game.getRewardB();
		MatlabProxyFactory factory = new MatlabProxyFactory();
	    MatlabProxy proxy = null;
		try {
			proxy = factory.getProxy();
		} catch (MatlabConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		double[] U = learn(Ra,proxy);
		Agent A = new BestExpectedValueAgent(DeckFactory.getDeck(deckA), U, game.getReverseJumpTable());
		U = learn(Rb,proxy);
		Agent B = new BestExpectedValueAgent(DeckFactory.getDeck(deckB), U, game.getReverseJumpTable());
		game.SetAgents(A, B);
		System.out.println("Game Starting With A as AI and B as Random Agents");
		System.out.println("====================================");
	}
	public void createHumanVsAi(Decks deckA, Decks deckB)
	{
		game = new Game();
		game.generateJumpTables();
		List<Card> deckOfA = DeckFactory.getDeck(deckA);
		List<Card> deckOfB = DeckFactory.getDeck(deckB);
		game.setRewardVectors(deckOfA, deckOfB);
		double [] Ra = this.game.getRewardA();
		MatlabProxyFactory factory = new MatlabProxyFactory();
	    MatlabProxy proxy = null;
		try {
			proxy = factory.getProxy();
		} catch (MatlabConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		double[] U = learn(Ra,proxy);
		Agent A = new MinimaxAgent(DeckFactory.getDeck(deckA), U, game.getReverseJumpTable());
		Agent B = new HumanAgent(deckB);
		game.SetAgents(A, B);
		System.out.println("Game Starting With A as AI and B as Human Agents");
		System.out.println("====================================");
	}
	public void playGame()
	{
		while (game.getCurrentTurn() < game.getNumberOfTurns()) {
//			System.out.println("Gamestate is: " + game.getHash());
			double[] where = whereCanIGo(game.getHash());
			try {
				game.nextPick(where);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
//		System.out.println("Gamestate is: " + game.getHash());
		if(Player.A == game.showDown())
		{
			WinsOfA++;
//			System.out.println("A wins the game");	
		}
		else
		{
			WinsOfB++;
//			System.out.println("B wins the game");
		}
		
	}
	public double[] learn(double [] Ra,MatlabProxy proxy)
	{
		int numberOfStates = this.game.getNumberOfStates();
		double[] U = new double[numberOfStates];
	    double[] prevU = new double[numberOfStates];
	    double [] Rb = this.game.getRewardB();
	    for (int i = 0; i < U.length; i++) {
			U[i] = 0;
			prevU[i] = 0.1; 
		}
		try {
			proxy.setVariable("Ra", Ra);
		    proxy.setVariable("Rb", Rb);
		    proxy.setVariable("numberOfStates", numberOfStates);
		    MatlabTypeConverter processor = new MatlabTypeConverter(proxy);
		    double[][] modifiedInP = new double[numberOfStates][49];
		    whereCanIGo = new double[numberOfStates][49];
		    proxy.setVariable("U", U);
		    double diff = 100;
			while(diff > 0d)
			{
				diff = 0;
				processor.setNumericArray("modifiedInP", new MatlabNumericArray(modifiedInP, null));
				processor.setNumericArray("whereCanIGo", new MatlabNumericArray(whereCanIGo, null));	
				proxy.eval("U = ADPlearn(numberOfStates,Ra,modifiedInP,whereCanIGo);");
			    U = (double[]) proxy.getVariable("U");
			    for (int j = 0; j < prevU.length; j++) {
			    	if(Double.isNaN(U[j]) || Double.isInfinite(U[j]))
			    	{
			    		U[j] = 0;
			    	}
			    	diff += Math.abs(prevU[j] - U[j]);
				}
			    prevU = U;
//			    System.out.println(diff);
				modifiedInP = getModifiedInP(U);
			}
		} catch (MatlabInvocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		for (int i = 0; i < U.length; i++) {
//			U[i] = U[i] + Ra[i]; 
//		}
		return U;
		
	}
	public double[][] getModifiedInP(double[] U)
	{
		int numberOfStates = this.game.getNumberOfStates();		    
		double [][] changedValues = new double[numberOfStates][49];
		int maxValue = 0;
		int NumberOfmaxValue = 0;
		whereCanIGo = new double[numberOfStates][49];
		for (int i = 44100; i < numberOfStates; i++) {
			whereCanIGo[i] = whereCanIGo(i);
			if(whereCanIGo != null)
			{
				for (int j = 0; j < whereCanIGo[0].length && whereCanIGo[i][j] != -1; j++) {
					if(maxValue < whereCanIGo[i][j])
					{
						maxValue = (int) whereCanIGo[i][j];
					}
					if(maxValue == whereCanIGo[i][j])
					{
						NumberOfmaxValue++;
					}
				}
				for (int j = 0; j < whereCanIGo[0].length && whereCanIGo[i][j] != -1; j++) {
					if(U[(int) whereCanIGo[i][j]] == maxValue)
					{
						changedValues[i][j] = 0.8d/(double)NumberOfmaxValue; 
					}
					else
					{
						changedValues[i][j] = 0.2d/(double)(whereCanIGo[i][j] - NumberOfmaxValue);
					}
				}
				maxValue = 0;
				NumberOfmaxValue = 0;
			}
		}
		
		return changedValues;
	}
	public double[] whereCanIGo(int from)
	{
		double[] go = new double[49];
		int currentState = this.game.getReverseJumpTable()[from];
		String currentStateAsString = Integer.toString(currentState);
		for (int i = 0; i < go.length; i++) {
			go[i] = -1;
		}
		if(from <= this.game.getLast3CardFirstNull())
		{
			return null;
		}
		for (int i = 0,j = 0; i < go.length; i++) {
			String nextPicks = Integer.toString(i);
			if(i < 10)
			{
				nextPicks = "0" + nextPicks;
			}
			if(nextPicks.charAt(0) < '7' && nextPicks.charAt(1) < '7')
			{
//				System.out.println("CurrentState: " + currentStateAsString);
//				System.out.println("From" + from);
//				System.out.println("Nextpick" + nextPicks);
				if(from > this.game.getLast3CardFirstNull() && from <= this.game.getLast2CardFirstNotNull()
						&& currentStateAsString.charAt(0) != nextPicks.charAt(0) && currentStateAsString.charAt(1) != nextPicks.charAt(0)
						&& currentStateAsString.charAt(2) != nextPicks.charAt(1) && currentStateAsString.charAt(3) != nextPicks.charAt(1))
				{
					int jumpIndex = Integer.parseInt(currentStateAsString.substring(0, 2) + nextPicks.charAt(0) + currentStateAsString.substring(2) + nextPicks.charAt(1));
					int test = this.game.getJumpTable3Card()[jumpIndex];
					go[j] = this.game.getJumpTable3Card()[jumpIndex]; /// kezelni kell ha nem val� sz�m van
					j++;
				}
				if(from > this.game.getLast2CardFirstNotNull() && from <= this.game.getLast2CardFirstNull()
						&& currentStateAsString.charAt(0) != nextPicks.charAt(0)
						&& currentStateAsString.charAt(1) != nextPicks.charAt(1) && currentStateAsString.charAt(2) != nextPicks.charAt(1))
				{
					int jumpIndex = Integer.parseInt("" + currentStateAsString.charAt(0) + nextPicks.charAt(0) + currentStateAsString.substring(1) + nextPicks.charAt(1));
					go[j] = this.game.getJumpTable3CardFirst0()[jumpIndex];
					j++;
				}
				if(from > this.game.getLast2CardFirstNull() && from <= this.game.getLast1CardFirstNotNull()
						&& currentStateAsString.charAt(0) != nextPicks.charAt(0) && currentStateAsString.charAt(1) != nextPicks.charAt(1))
				{
					int jumpIndex = Integer.parseInt("" + currentStateAsString.charAt(0) + nextPicks.charAt(0) + currentStateAsString.charAt(1) + nextPicks.charAt(1));
					go[j] = this.game.getJumpTable2Card()[jumpIndex];
					j++;
				}
				if(from > this.game.getLast1CardFirstNotNull() && from <= this.game.getLast1CardFirstNull() && currentStateAsString.charAt(0) != nextPicks.charAt(1))
				{	
					int jumpIndex = Integer.parseInt(nextPicks.charAt(0) + currentStateAsString + nextPicks.charAt(1));
					go[j] = this.game.getJumpTable2CardFirst0()[jumpIndex];
					j++;
				}
			}
		}
		
		if(from == game.getLast1CardFirstNull() + 1)
		{
			
			for (int i = 0, k = this.game.getLast2CardFirstNull() + 1; i < go.length; i++, k++) {
				go[i] = k;
			}
		}
		return go;
	}
	public void balance()
	{
		MatlabProxyFactory factory = new MatlabProxyFactory();
	    MatlabProxy proxy = null;
		try {
			proxy = factory.getProxy();
		} catch (MatlabConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int iter = 0;
		boolean cardAdjustmentNeeded = true; 
		List<Card> deckOfA = null;
		List<Card> deckOfB = null;
		int maxIndex = 1000;
		double maxValue = 0;
		Player whosDeck;
		Card cardToAdjust;
		TreeMap<Integer,Ability> dist;
		List<Integer> WinsStatA = new ArrayList<Integer>();
		List<Integer> WinsStatB = new ArrayList<Integer>();
		Card prevCard = new Card(0, 0, 0, 0, 0);
		while(cardAdjustmentNeeded)
		{
			if(iter == 0)
			{
				deckOfA = DeckFactory.getDeck(Decks.RANDOM);
				deckOfB = DeckFactory.getDeck(Decks.RANDOM);
			}
			cardAdjustmentNeeded = false;
			System.out.println(iter++);
			game = new Game();
			game.generateJumpTables();
			game.setRewardVectors(deckOfA, deckOfB);
			double [] Ra = this.game.getRewardA();
			double [] Rb = this.game.getRewardB();
			
			double[] Ua = learn(Ra,proxy);
			double[] Ub = learn(Rb,proxy);
			double[] utilityOfCardsFromA = calcCardUtility(Ua, this.game.getReverseJumpTable(), Player.A);
			double[] utilityOfCardsFromB = calcCardUtility(Ub, this.game.getReverseJumpTable(), Player.B);
			
			Agent A = new BestExpectedValueAgent(deckOfA, Ua, game.getReverseJumpTable());
			Agent B = new BestExpectedValueAgent(deckOfB, Ub, game.getReverseJumpTable());
			game.SetAgents(A, B);
			WinsOfA = 0;
			WinsOfB = 0;
			for (int i = 0; i < 1000; i++) {
				playGame();
			}
			for (Iterator iterator = deckOfA.iterator(); iterator.hasNext();) {
				Card card = (Card) iterator.next();
				System.out.println("DeckA: " + card + "	" + utilityOfCardsFromA[card.getID()]);
			}
			for (Iterator iterator = deckOfB.iterator(); iterator.hasNext();) {
				Card card = (Card) iterator.next();
				System.out.println("DeckB: " + card +"	"+ utilityOfCardsFromB[card.getID()]);
			}
			WinsStatA.add(WinsOfA);
			WinsStatB.add(WinsOfB);
			System.out.println("Number of Times A Won: " + WinsOfA);
			System.out.println("Number of Times B Won: " + WinsOfB);
			if(Math.abs(WinsOfA - WinsOfB) > 150)
			{
				maxValue = 0;
				cardAdjustmentNeeded = true;
				whosDeck = Player.A;
				for (int i = 0; i < utilityOfCardsFromA.length; i++) {
					if(maxValue < Math.abs(utilityOfCardsFromA[i]))
					{
						maxValue = Math.abs(utilityOfCardsFromA[i]);
						maxIndex = i;
					}
				}
				for (int i = 0; i < utilityOfCardsFromB.length; i++) {
					if(maxValue < Math.abs(utilityOfCardsFromB[i]))
					{
						maxValue = Math.abs(utilityOfCardsFromB[i]);
						maxIndex = i;
						whosDeck = Player.B;
					}
				}
				int adjustMent = 0;
				if(Math.random() * 100 < 10)
				{
					maxIndex = (int)(Math.random()*deckOfA.size());
				}
				if(whosDeck == Player.A)
				{
					cardToAdjust = deckOfA.get(maxIndex);
					System.out.println("I'm adjusting: " + cardToAdjust.toString());
					dist = deckDistance(deckOfA, cardToAdjust);
					if(WinsOfA > WinsOfB)
					{
						adjustMent = -1;
					}
					else
					{
						adjustMent = 1;
					}
				
				}
				else
				{
					cardToAdjust = deckOfB.get(maxIndex);
					if(prevCard.equals(cardToAdjust))
					{
						System.out.println("baj van");
					}
					prevCard = cardToAdjust; 
					System.out.println("I'm adjusting: " + cardToAdjust.toString());
					dist = deckDistance(deckOfB, cardToAdjust);
					if(WinsOfA > WinsOfB)
					{
						adjustMent = 1;
					}
					else
					{
						adjustMent = -1;
					}
				}
				boolean adjustmentDone = false;
				for(Entry<Integer, Ability> entry : dist.entrySet()) {
					  Integer key = entry.getKey();
					  Ability ability = entry.getValue();
					  if(adjustmentDone == true)
					  {
						  break;
					  }
					  switch (ability) {
						case ALFA:
							if((cardToAdjust.getAlfa() > 0 && adjustMent == -1 ) || (adjustMent == 1 && cardToAdjust.getAlfa() < 9))
							{
								cardToAdjust.setAlfa(cardToAdjust.getAlfa() + adjustMent);
								adjustmentDone = true;
							}
							break;
						case ARMOR:
							if((cardToAdjust.getArmor() > 0 && adjustMent == -1 ) || (adjustMent == 1 && cardToAdjust.getArmor() < 9))
							{
								cardToAdjust.setArmor(cardToAdjust.getArmor() + adjustMent);
								adjustmentDone = true;
							}
							break;
						case SPEED:
							if((cardToAdjust.getSpeed() > 0 && adjustMent == -1 ) || (adjustMent == 1 && cardToAdjust.getSpeed() < 9))
							{
								cardToAdjust.setSpeed(cardToAdjust.getSpeed() + adjustMent);
								adjustmentDone = true;
							}
							break;
						case ROF:
							if((cardToAdjust.getRof() > 0 && adjustMent == -1 ) || (adjustMent == 1 && cardToAdjust.getRof() < 9))
							{
								cardToAdjust.setRof(cardToAdjust.getRof() + adjustMent);
								adjustmentDone = true;
							}
							break;
						default:
							System.out.println("Baj van!");
							break;
					  }
				}
				
				
			}
		}
		int WinsStatAFinal[] = new int[WinsStatA.size()];
		int WinsStatBFinal[] = new int[WinsStatB.size()];
		for (int i = 0; i < WinsStatAFinal.length; i++) {
			WinsStatAFinal[i] = WinsStatA.get(i);
			WinsStatBFinal[i] = WinsStatB.get(i);
		}
		try {
			proxy.setVariable("WinsStatA", WinsStatAFinal);
			proxy.setVariable("WinsStatB", WinsStatBFinal);
		} catch (MatlabInvocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		proxy.disconnect();
	}
	public double[] calcCardUtility(double[] U, int[] reverseJumpTable,Player whoAmI)
	{
		double[] deck = new double[7];
		for (int i = 0; i < deck.length; i++) {
			deck[i] = 0;
		}
		String desiredState;
		char pick = 'a';
		for (int i = 0; i < U.length; i++) {
			desiredState = Integer.toString(reverseJumpTable[i]);
			if(whoAmI == Player.A)
			{	
				switch (desiredState.length()) {
				case 1:
					pick = '0';
					break;
				case 2:
					pick = desiredState.charAt(0);
					break;
				case 3:
					pick = desiredState.charAt(0);
					break;
				case 4:
					pick = desiredState.charAt(1);
					break;
				case 5:
					pick = desiredState.charAt(1);
					break;
				case 6:
					pick = desiredState.charAt(2);
					break;
				default:
					pick = 'a';
					break;
				}
			}
			if(whoAmI == Player.B)
			{	
				pick = desiredState.charAt(desiredState.length()-1);
			}
			int deckNumber = (int)pick - (int)'0';
			if(i != 45913)
			{
				deck[deckNumber] += U[i];
			}
		}
		return deck;
	}
	public TreeMap<Integer,Ability> deckDistance(List<Card> deck, Card c)
	{
		TreeMap<Integer,Ability> distances = new TreeMap();
		Card distance = new Card(0,0,0,0,0);
		for (Iterator iterator = deck.iterator(); iterator.hasNext();) {
			Card card = (Card) iterator.next();
			distance.setAlfa(distance.getAlfa() + card.getAlfa() - c.getAlfa());
			distance.setAlfa(distance.getRof() + card.getRof() - c.getRof());
			distance.setAlfa(distance.getSpeed() + card.getSpeed() - c.getSpeed());
			distance.setAlfa(distance.getArmor() + card.getArmor() - c.getArmor());
		}
		distances.put(distance.getAlfa(), Ability.ALFA);
		distances.put(distance.getRof(), Ability.ROF);
		distances.put(distance.getSpeed(), Ability.SPEED);
		distances.put(distance.getArmor(), Ability.ARMOR);
		return distances;
	}
	
}
