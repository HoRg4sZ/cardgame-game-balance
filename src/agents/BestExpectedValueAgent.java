package agents;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import world.Card;
import world.Decks;
import world.Player;

public class BestExpectedValueAgent extends Agent{
	private double[] U;
	private int[] reverseJumpTable;
	public BestExpectedValueAgent(List<Card> d,double[] U, int[] reverseJumpTable) {
		super(d);
		this.U = U;
		this.reverseJumpTable = reverseJumpTable;
	}
	@Override
	public Card playCard(List<Card> myCardsOnTable, List<Card> opponentsCardsOnTable, double[] whereCanIGo, Player whoAmI) {
		double [] utilitySum = new double[7];
		for (int i = 0; i < utilitySum.length; i++) {
			utilitySum[i] = 0;
		}
		for (int i = 0; i < whereCanIGo.length; i++) {
			int next = (int) whereCanIGo[i];
			if(next != -1)
			{
				String desiredState = Integer.toString(reverseJumpTable[next]);
				if(desiredState.length() % 2 != 0)
				{
					desiredState = "0" + desiredState;
				}
				char pick = 'a';
				if(whoAmI == Player.A)
				{	
					switch (myCardsOnTable.size()) {
					case 0:
						pick = desiredState.charAt(0);
						break;
					case 1:
						pick = desiredState.charAt(1);
						break;
					case 2:
						pick = desiredState.charAt(2);
						break;
					default:
						pick = 'a';
						break;
					}
				}
				if(whoAmI == Player.B)
				{	
					switch (myCardsOnTable.size()) {
					case 0:
						pick = desiredState.charAt(1);
						break;
					case 1:
						pick = desiredState.charAt(3);
						break;
					case 2:
						pick = desiredState.charAt(5);
						break;
					default:
						pick = 'a';
						break;
					}
				}
				int deckNumber = (int)pick - (int)'0';
				double j = U[(int) whereCanIGo[i]];
				utilitySum[deckNumber] += j;
			}			
		}
		double UtilTemp = -100;
		double [] utilityOrder = new double[7];
		for (int i = 0; i < utilitySum.length; i++) {
			utilityOrder[i] = utilitySum[i];
		}
		boolean wasChange = true;
		while(wasChange == true)
		{
			wasChange = false;
			for (int j = 0; j < utilitySum.length - 1; j++) {
				if(utilitySum[j] < utilitySum[j + 1])
				{
					UtilTemp = utilitySum[j];
					utilitySum[j] = utilitySum[j + 1];
					utilitySum[j + 1] = UtilTemp;
					wasChange = true;
				}
			}
		}
		int chosenIndex = -100;
		int rand = (int)(Math.random()*100);
		double wannaFind;
		Card theChosenOne = null;
		if(rand < 80)
		{
			theChosenOne = findBestCardInDeck(utilitySum, utilityOrder, 0);
		}
		else
		{
			if(rand < 85)
			{
				theChosenOne = findBestCardInDeck(utilitySum, utilityOrder, 1);
			}
			if(rand >= 85 && rand < 90)
			{
				theChosenOne = findBestCardInDeck(utilitySum, utilityOrder, 2);
			}
			if(rand >= 90 && rand < 95)
			{
				theChosenOne = findBestCardInDeck(utilitySum, utilityOrder, 3);
			}
			if(rand >= 95)
			{
				theChosenOne = deck.get((int)(Math.random()*deck.size()));
			}
			
		}
		
		deck.remove(theChosenOne);
		return theChosenOne;

	}
	private Card findBestCardInDeck(double [] utilitySum, double [] utilityOrder, int offset)
	{
		double wannaFind;
		Card theChosenOne = null;
		for(int i = offset; theChosenOne == null; i++)
		{
			if(i == 6)
			{
				System.out.println("mukk");
			}
			wannaFind = utilitySum[i];
			int chosenIndex = -100;
			for (int j = 0; j < utilitySum.length; j++) {
				if(wannaFind == utilityOrder[j])
				{
					chosenIndex = j;
					utilityOrder[j] = - 10000000000d;
					j = utilitySum.length;
				}
			}
			for (Iterator iterator = deck.iterator(); iterator.hasNext();) {
				Card card = (Card) iterator.next();
				if(card.getID() == chosenIndex)
				{
					theChosenOne = card;
				}
			}
		}
		return theChosenOne;
	}	
}
