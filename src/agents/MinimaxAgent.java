package agents;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import world.Card;
import world.Decks;
import world.Player;

public class MinimaxAgent extends Agent{
	private double[] U;
	private int[] reverseJumpTable;
	public MinimaxAgent(List<Card> d,double[] U, int[] reverseJumpTable) {
		super(d);
		this.U = U;
		this.reverseJumpTable = reverseJumpTable;
	}
	@Override
	public Card playCard(List<Card> myCardsOnTable, List<Card> opponentsCardsOnTable, double[] whereCanIGo, Player whoAmI) {
		double [] utilitySortedByMove = new double[7];
		for (int i = 0; i < utilitySortedByMove.length; i++) {
			utilitySortedByMove[i] = 100;
		}
		for (int i = 0; i < whereCanIGo.length; i++) {
			int next = (int) whereCanIGo[i];
			if(next != -1)
			{
				String desiredState = Integer.toString(reverseJumpTable[next]);
				if(desiredState.length() % 2 != 0)
				{
					desiredState = "0" + desiredState;
				}
				char pick = 'a';
				if(whoAmI == Player.A)
				{	
					switch (myCardsOnTable.size()) {
					case 0:
						pick = desiredState.charAt(0);
						break;
					case 1:
						pick = desiredState.charAt(1);
						break;
					case 2:
						pick = desiredState.charAt(2);
						break;
					default:
						pick = 'a';
						break;
					}
				}
				if(whoAmI == Player.B)
				{	
					switch (myCardsOnTable.size()) {
					case 0:
						pick = desiredState.charAt(1);
						break;
					case 1:
						pick = desiredState.charAt(3);
						break;
					case 2:
						pick = desiredState.charAt(5);
						break;
					default:
						pick = 'a';
						break;
					}
				}
				int myPick = (int)pick - (int)'0';
				double j = U[(int) whereCanIGo[i]];
				if(utilitySortedByMove[myPick] > j)
				{
					utilitySortedByMove[myPick] = j;
				}
			}			
		}
		int maxIndex = -100;
		double maxUtil = -100;
		for (int j = 0; j < utilitySortedByMove.length; j++) {
			if(maxUtil < utilitySortedByMove[j])
			{
				maxUtil = utilitySortedByMove[j];
				maxIndex = j;
			}
		}
	Card theChosenOne = null;
	for (Iterator iterator = deck.iterator(); iterator
			.hasNext();) {
		Card card = (Card) iterator.next();
		if(card.getID() == maxIndex)
		{
			theChosenOne = card;
		}
	}
//	if(theChosenOne == null)
//	{
//		theChosenOne = deck.get((int)(Math.random()*deck.size()));
//	}
	deck.remove(theChosenOne);
	return theChosenOne;
		
//		boolean wasSwitch = true;
//		double temp;
//		int nextState;
//		int stateAfterNext;
//		while(wasSwitch)
//		{
//			wasSwitch = false;
//			for (int i = 0; i < whereCanIGo.length - 1 ; i++) {
//				nextState = (int) whereCanIGo[i];
//				stateAfterNext = (int) whereCanIGo[i + 1];
//				if(stateAfterNext != -1 && nextState != -1)
//				{
//					if(U[(int) whereCanIGo[i]] < U[(int) whereCanIGo[i + 1]])
//					{
//						
//						temp = whereCanIGo[i];
//						whereCanIGo[i] = whereCanIGo[i + 1];
//						whereCanIGo[i + 1] = temp;
//						wasSwitch = true;
//					}	
//				}
//			}
//		}
//		int pickNumber = (int) whereCanIGo[2];
////		List<Integer> maxStates = new ArrayList<Integer>();
////		double maxUtil = 0;
////		for (int i = 0; i < whereCanIGo.length; i++) {
////			int nextState = (int) whereCanIGo[i];
////			if(nextState != -1)
////			{
////				if(maxUtil < U[nextState])
////				{
////					maxUtil = U[nextState];
////					maxStates.clear();
////					maxStates.add(nextState);
////				}
////				if(maxUtil == U[nextState])
////				{
////					maxStates.add(nextState);
////				}
////			}
////		}
////		int rand = (int)(Math.random()*maxStates.size());
////		int pickNumber = maxStates.get(rand);
//		String desiredState = Integer.toString(reverseJumpTable[pickNumber]);
//		if(desiredState.length() % 2 != 0)
//		{
//			desiredState = "0" + desiredState;
//		}
//		char pick = 'a';
//		if(whoAmI == Player.A)
//		{	
//			switch (myCardsOnTable.size()) {
//			case 0:
//				pick = desiredState.charAt(0);
//				break;
//			case 1:
//				pick = desiredState.charAt(1);
//				break;
//			case 2:
//				pick = desiredState.charAt(2);
//				break;
//			default:
//				pick = 'a';
//				break;
//			}
//		}
//		if(whoAmI == Player.B)
//		{	
//			switch (myCardsOnTable.size()) {
//			case 0:
//				pick = desiredState.charAt(1);
//				break;
//			case 1:
//				pick = desiredState.charAt(3);
//				break;
//			case 2:
//				pick = desiredState.charAt(5);
//				break;
//			default:
//				pick = 'a';
//				break;
//			}
//		}
//		int deckNumber = (int)pick - (int)'0';
//		return deck.get(deckNumber);
	}

}
