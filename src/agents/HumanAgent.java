package agents;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.List;

import world.Card;
import world.DeckFactory;
import world.Decks;
import world.Player;

public class HumanAgent extends Agent{

	BufferedReader br;
	public HumanAgent(Decks d)
	{
		deck = DeckFactory.getDeck(d);
		br = new BufferedReader(new InputStreamReader(System.in));
	}
	@Override
	public Card playCard(List<Card> myCardsOnTable, List<Card> opponentsCardsOnTable, double[] whereCanIGo, Player whoAmI) throws IOException {
		System.out.println("Opponents cards on table:");
		for (Iterator iterator = opponentsCardsOnTable.iterator(); iterator
				.hasNext();) {
			Card card = (Card) iterator.next();
			System.out.println(card.toString());
		}
		System.out.println("My cards on table: ");
		for (Iterator iterator = myCardsOnTable.iterator(); iterator
				.hasNext();) {
			Card card = (Card) iterator.next();
			System.out.println(card.toString());
		}
		System.out.println("Available cards(armor, alfa, rof, speed): ");
		for (Iterator iterator = deck.iterator(); iterator
				.hasNext();) {
			Card card = (Card) iterator.next();
			System.out.println(card.toString());
		}
		String in = br.readLine();
		int cardNumber = Integer.parseInt(in);
		Card temp = new Card(0,0,0, 0, 0);
		for (Iterator iterator = deck.iterator(); iterator
				.hasNext();) {
			Card card = (Card) iterator.next();
			if(card.getID() == cardNumber)
			{
				temp = card;
			}
		}
		deck.remove(temp);
		return temp;
	}
	
}
