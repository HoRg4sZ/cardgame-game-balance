package agents;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import world.Card;
import world.DeckFactory;
import world.Decks;
import world.Player;

public abstract class Agent {						// abstract class for Agents 
	protected List<Card> deck;
	protected List<Card> backUpDeck;
	protected List<Card> cardsOnTheTable;
	public Agent()
	{
		
	}
	protected Agent(List<Card> d)
	{
		deck = d;
		backUpDeck = new ArrayList<Card>();
		for (Iterator iterator = d.iterator(); iterator.hasNext();) {
			Card card = (Card) iterator.next();
			backUpDeck.add(new Card(card));
		}
	}
	public abstract Card playCard(List<Card> myCardsOnTable, List<Card> opponentsCardsOnTable, double[] whereCanIGo, Player whoAmI) throws IOException;

	public void resetsetDeck()
	{
		deck.clear();
		for (Iterator iterator = backUpDeck.iterator(); iterator.hasNext();) {
			Card card = (Card) iterator.next();
			deck.add(new Card(card));
		}
	}
	
}
