package agents;

import java.util.List;

import world.Card;
import world.Decks;
import world.Player;

public class RandomAgent extends Agent {					// pick a random card in every turn

	public RandomAgent(List<Card> d)
	{
		super(d);
	}
	
	@Override
	public Card playCard(List<Card> myCardsOnTable, List<Card> opponentsCardsOnTable, double[] whereCanIGo, Player whoAmI) {
		int rand = (int)(Math.random()*deck.size());
		Card card = deck.get(rand);
		deck.remove(rand);
		return card;
	}

}
