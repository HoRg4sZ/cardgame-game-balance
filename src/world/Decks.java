package world;

public enum Decks {				// enumeration, for possible decks
	DPM,
	ARMOR,
	SPEED,
	ALLAVARAGE,
	FASTANDALFA,
	ARMORANDALFA,
	RANDOM
}
