package world;

public class Card {
	private int armor;					// simple card class to store the values of a card, each attribute can change form 1-10
	private int alfa;
	private int rof;
	private int speed;
	private int ID;
	public Card(int armor, int alfa, int rof, int speed, int ID) {
		super();
		this.armor = armor;
		this.alfa = alfa;
		this.rof = rof;
		this.speed = speed;
		this.ID = ID;
	}
	public Card(Card c)
	{
		this(c.getArmor(),c.getAlfa(),c.getRof(),c.getSpeed(),c.getID());
	}
	public int getID()
	{
		return ID;
	}
	public int getArmor() {
		return armor;
	}
	public int getAlfa() {
		return alfa;
	}
	public int getRof() {
		return rof;
	}
	public int getSpeed() {
		return speed;
	}
	
	public void setArmor(int armor) {
		this.armor = armor;
	}
	public void setAlfa(int alfa) {
		this.alfa = alfa;
	}
	public void setRof(int rof) {
		this.rof = rof;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public String toString()
	{
		String me = "Card " + ID + ": " + armor + alfa + rof + speed;  
		return me;
	}
	
	
}
