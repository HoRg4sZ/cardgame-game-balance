package world;

import java.util.ArrayList;
import java.util.List;

public class DeckFactory {							// factory class to create specific deck
	static public final int deckSize = 7;
	public static List<Card> getDeck(Decks d)
	{
		List<Card> deck = new ArrayList<Card>();
		switch (d) {
		case DPM:
			deck.add(new Card(1,8,7,1,0));
			deck.add(new Card(1,7,5,1,1));
			deck.add(new Card(1,3,3,6,2));
			deck.add(new Card(1,8,2,1,3));
			deck.add(new Card(1,2,8,1,4));
			deck.add(new Card(4,3,3,1,5));
			deck.add(new Card(2,4,4,2,6));
			break;
		case ARMOR:
			deck.add(new Card(9,1,1,1,0));
			deck.add(new Card(6,1,2,1,1));
			deck.add(new Card(3,2,1,2,2));
			deck.add(new Card(2,2,3,1,3));
			deck.add(new Card(3,1,1,6,4));
			deck.add(new Card(6,1,2,1,5));
			deck.add(new Card(4,3,1,2,6));
			break;
		case ALLAVARAGE:
			for (int i = 0; i < 7; i++) {
				deck.add(new Card(5,5,5,5,i));
			}
			break;
		case SPEED:
			for (int i = 0; i < 7; i++) {
				deck.add(new Card(1,1,1,9,i));
			}
			break;
		case FASTANDALFA:	
			for (int i = 0; i < 7; i++) {
				deck.add(new Card(1,7,1,7,i));
			}
			break;
		case ARMORANDALFA:
			for (int i = 0; i < 7; i++) {
				deck.add(new Card(7,7,1,1,i));
			}
			break;
		case RANDOM:
			for (int i = 0; i < deckSize; i++) {
				deck.add(new Card(1+(int)(Math.random()*8),1+(int)(Math.random()*8),1+(int)(Math.random()*8),1+(int)(Math.random()*8),i));
			}
			break;
		}
		return deck;		
	}
}
