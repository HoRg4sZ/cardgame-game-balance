package world;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import world.Card;
import agents.Agent;

public class Game {									// the class, that represents one game
	private Agent agentA;
	private int[] jumpTable3Card;
	private int[] jumpTable3CardFirst0;
	private int[] jumpTable2Card;
	private int[] jumpTable2CardFirst0;
	private int[] jumpTable1Card;
	private int[] jumpTable1CardFirst0;
	private int numberOfStates;
	private int[] reverseJumpTable;
	private int last3CardFirstNotNull;
	private int last3CardFirstNull;
	private int last2CardFirstNotNull;
	private int last2CardFirstNull;
	private int last1CardFirstNotNull;
	private int last1CardFirstNull;
	private double[] rewardA;
	private double[] rewardB;
	private Agent agentB;
	private List<Card> cardsOnTableFromA;
	private List<Card> cardsOnTableFromB;
	private final int numberOfTurns = 3;
	private int currentTurn;
	public double[] getRewardA() {
		return rewardA;
	}
	public double[] getRewardB() {
		return rewardB;
	}
	public int getNumberOfStates() {
		return numberOfStates;
	}
	public Agent getAgentA() {
		return agentA;
	}
	public Agent getAgentB() {
		return agentB;
	}
	public Game(Agent A, Agent B)
	{
		agentA = A;
		agentB = B;
		cardsOnTableFromA = new ArrayList<Card>();
		cardsOnTableFromB = new ArrayList<Card>();
		currentTurn = 0;
		generateJumpTables();
	}
	public Game()
	{
		cardsOnTableFromA = new ArrayList<Card>();
		cardsOnTableFromB = new ArrayList<Card>();
		currentTurn = 0;
	}
	public void SetAgents(Agent A, Agent B)
	{
		agentA = A;
		agentB = B;
	}
	public void nextPick(double[] whereCanIGo) throws IOException							// asking agents, for next move - picking next cards
	{
		//List<Card> myCardsOnTable, List<Card> opponentsCardsOnTable, Decks enemyDeckType, int[] whereCanIGo, Player whoAmI
		Card playedByA = agentA.playCard(cardsOnTableFromA,cardsOnTableFromB, whereCanIGo, Player.A);
		Card playedByB = agentB.playCard(cardsOnTableFromB,cardsOnTableFromA, whereCanIGo, Player.B);
//		System.out.println("A played: " + playedByA.getID());
//		System.out.println("B played: " + playedByB.getID());
		cardsOnTableFromA.add(playedByA);
		cardsOnTableFromB.add(playedByB);
		currentTurn++;
	}
	public int[] getJumpTable3Card() {
		return jumpTable3Card;
	}
	public int getLast3CardFirstNotNull() {
		return last3CardFirstNotNull;
	}
	public int getLast3CardFirstNull() {
		return last3CardFirstNull;
	}
	public int getLast2CardFirstNotNull() {
		return last2CardFirstNotNull;
	}
	public int getLast2CardFirstNull() {
		return last2CardFirstNull;
	}
	public int getLast1CardFirstNotNull() {
		return last1CardFirstNotNull;
	}
	public int getLast1CardFirstNull() {
		return last1CardFirstNull;
	}
	public int[] getJumpTable3CardFirst0() {
		return jumpTable3CardFirst0;
	}
	public int[] getJumpTable2Card() {
		return jumpTable2Card;
	}
	public int[] getJumpTable2CardFirst0() {
		return jumpTable2CardFirst0;
	}
	public int[] getJumpTable1Card() {
		return jumpTable1Card;
	}
	public int[] getJumpTable1CardFirst0() {
		return jumpTable1CardFirst0;
	}
	public int[] getReverseJumpTable() {
		return reverseJumpTable;
	}
	public int getCurrentTurn() {
		return currentTurn;
	}
	public int getNumberOfTurns() {
		return numberOfTurns;
	}
	public Player showDown()  						// evaluate the current game, after a specific number of cards were drawn 
	{
		double scoreA = 0;
		double scoreB = 0;
		int dpmA = 0;
		int dpmB = 0;
		int armorA = 0;
		int armorB = 0;
		int maxSpeed = 0;
		int maxArmor = 0;
		int speedA = 0;
		int speedB = 0;
		Card cardWithMaxSpeed = new Card(0,0,0,0,0);
		Card cardWithMaxSpeedA = new Card(0,0,0,0,0);
		Card cardWithMaxSpeedB = new Card(0,0,0,0,0);
		Card cardWithMaxArmorA = new Card(0,0,0,0,0);
		Card cardWithMaxArmorB = new Card(0,0,0,0,0);
		Card cardWithMaxArmor = new Card(0,0,0,0,0);
		for (Iterator<Card> iterator = cardsOnTableFromA.iterator(); iterator
				.hasNext();) {
			Card card = (Card) iterator.next();
			dpmA += card.getAlfa()*card.getRof();
			armorA += card.getArmor();
			int speed = card.getSpeed();
			speedA += speed;
			if(maxSpeed < speed)
			{
				maxSpeed = speed;
				cardWithMaxSpeedA = card;
			}
			int armor = card.getArmor();
			if(maxArmor < armor)
			{
				maxArmor = armor;
				cardWithMaxArmorA = card;
			}
		}
		for (Iterator<Card> iterator = cardsOnTableFromB.iterator(); iterator
				.hasNext();) {
			Card card = (Card) iterator.next();
			dpmB += card.getAlfa()*card.getRof();
			armorB += card.getArmor();
			int speed = card.getSpeed();
			speedB += speed;
			if(maxSpeed < speed)
			{
				maxSpeed = speed;
				cardWithMaxSpeedB = card;
			}
			int armor = card.getArmor();
			if(maxArmor < armor)
			{
				maxArmor = armor;
				cardWithMaxArmorB = card;
			}
		}
		if(cardWithMaxArmorA.getArmor() > cardWithMaxArmorB.getArmor())
		{
			cardWithMaxArmor = cardWithMaxArmorA;
		}
		else
		{
			cardWithMaxArmor = cardWithMaxArmorB;
		}
		if(cardWithMaxSpeedA.getSpeed() > cardWithMaxSpeedB.getSpeed())
		{
			cardWithMaxSpeed = cardWithMaxSpeedA;
		}
		else
		{
			cardWithMaxSpeed = cardWithMaxSpeedB;
		}
		scoreA += (double)dpmA/(double)armorB;
		scoreB += (double)dpmB/(double)armorA;
		if(cardsOnTableFromA.contains(cardWithMaxSpeed))
		{
			scoreA += (double)(cardWithMaxSpeed.getAlfa() * cardWithMaxSpeed.getRof()) / (double)3;
		}
		else
		{
			scoreB += (double)(cardWithMaxSpeed.getAlfa() * cardWithMaxSpeed.getRof()) / (double)3;
		}
		if(cardsOnTableFromA.contains(cardWithMaxArmor))
		{
			scoreA += cardWithMaxArmor.getAlfa() * 2; 
		}
		else
		{
			scoreB += cardWithMaxArmor.getAlfa() * 2;
		}
		if(speedA > speedB)
		{
			scoreA += speedA-speedB;
		}
		else
		{
			scoreB += speedB-speedA;
		}
		currentTurn = 0;
		cardsOnTableFromA.clear();
		cardsOnTableFromB.clear();
		if(agentA != null && agentB != null)
		{
			agentA.resetsetDeck();
			agentB.resetsetDeck();
//			System.out.println("Score of A: " + scoreA);
//			System.out.println("Score of B: " + scoreB);
		}
		if(scoreA > scoreB)
		{
			return Player.A;
		}
		else
		{
			return Player.B;
		}
	}
	public int getHash()
	{
		
		switch (cardsOnTableFromA.size()) {
		case 0:
			return numberOfStates - 1;
		case 1:
			if(cardsOnTableFromA.get(0).getID() == 0)
			{
				return jumpTable1CardFirst0[cardsOnTableFromB.get(0).getID()];
			}
			else
			{
				int index = cardsOnTableFromA.get(0).getID() * 10 + cardsOnTableFromB.get(0).getID();
				return jumpTable1Card[index];
			}
		case 2:
			if(cardsOnTableFromA.get(0).getID() == 0)
			{
				int index = cardsOnTableFromA.get(1).getID() * 100 + cardsOnTableFromB.get(0).getID() * 10 + cardsOnTableFromB.get(1).getID();
				return jumpTable2CardFirst0[index];
			}
			else
			{
				int index = cardsOnTableFromA.get(0).getID() * 1000 + cardsOnTableFromA.get(1).getID() * 100 + cardsOnTableFromB.get(0).getID() * 10 + cardsOnTableFromB.get(1).getID();
				return jumpTable2Card[index];
			}
		case 3:
			if(cardsOnTableFromA.get(0).getID() == 0)
			{
				int index = cardsOnTableFromA.get(1).getID() * 10000 + cardsOnTableFromA.get(2).getID() * 1000 + cardsOnTableFromB.get(0).getID() * 100 + cardsOnTableFromB.get(1).getID() * 10 + cardsOnTableFromB.get(2).getID();
				return jumpTable3CardFirst0[index];
			}
			else
			{
				int index = cardsOnTableFromA.get(0).getID() * 100000 + cardsOnTableFromA.get(1).getID() * 10000 + cardsOnTableFromA.get(2).getID() * 1000 + cardsOnTableFromB.get(0).getID() * 100 + cardsOnTableFromB.get(1).getID() * 10 + cardsOnTableFromB.get(2).getID();
				return jumpTable3Card[index];
			}
		default:
			return 100000;
		}
		
	}
	public int generateJumpTables()
	{
		jumpTable3Card = new int[654655];
		jumpTable3CardFirst0 = new int[65655];
		jumpTable2Card = new int[6566];
		jumpTable2CardFirst0 = new int[666];
		jumpTable1Card = new int[67];
		jumpTable1CardFirst0 = new int[7];
		reverseJumpTable = new int[45915];
		
		int jumpIndex = 0;
		
		for (int i = 100000; i < jumpTable3Card.length; i++) {
			String number = Integer.toString(i);
			if(number.charAt(0) != number.charAt(1) && number.charAt(0) != number.charAt(2) && number.charAt(1) != number.charAt(2) &&
					number.charAt(3) != number.charAt(4) && number.charAt(3) != number.charAt(5) && number.charAt(4) != number.charAt(5) && isAllCharSmallerThan7(number))
			{
				jumpTable3Card[i] = jumpIndex;
				reverseJumpTable[jumpIndex] = i;
				jumpIndex++;
			}
		}
		last3CardFirstNotNull = jumpIndex - 1;
//		System.out.println("Utols� 3 k�rty�s nem null�val kezd�d�: " +jumpIndex);
		for (int i = 10000; i < jumpTable3CardFirst0.length; i++) {
			String number = Integer.toString(i);
			if(number.charAt(0) != number.charAt(1) && number.charAt(2) != number.charAt(3) && number.charAt(2) != number.charAt(4) &&
					number.charAt(3) != number.charAt(4) && isAllCharSmallerThan7(number) && number.charAt(1) != '0')
			{
				jumpTable3CardFirst0[i] = jumpIndex;
				reverseJumpTable[jumpIndex] = i;
				jumpIndex++;
			}
		}
		last3CardFirstNull = jumpIndex - 1;
//		System.out.println("Utols� 3 k�rty�s null�val kezd�d�: " + jumpIndex);
		for (int i = 1000; i < jumpTable2Card.length; i++) {
			String number = Integer.toString(i);
			if(number.charAt(0) != number.charAt(1) && number.charAt(2) != number.charAt(3) && isAllCharSmallerThan7(number))
			{
				jumpTable2Card[i] = jumpIndex;
				reverseJumpTable[jumpIndex] = i;
				jumpIndex++;
			}
		}
		last2CardFirstNotNull = jumpIndex - 1;
//		System.out.println("Utols� 2 k�rty�s nem null�val kezd�d�: " +jumpIndex);
		for (int i = 100; i < jumpTable2CardFirst0.length; i++) {
			String number = Integer.toString(i);
			if(number.charAt(1) != number.charAt(2) && isAllCharSmallerThan7(number))
			{
				jumpTable2CardFirst0[i] = jumpIndex;
				reverseJumpTable[jumpIndex] = i;
				jumpIndex++;
			}
		}
		last2CardFirstNull = jumpIndex - 1;
//		System.out.println("Utols� 2 k�rty�s null�val kezd�d�: " +jumpIndex);
		for (int i = 10; i < jumpTable1Card.length; i++) {
			String number = Integer.toString(i);
			if(isAllCharSmallerThan7(number) && number.charAt(0) != '0')
			{
				jumpTable1Card[i] = jumpIndex;
				reverseJumpTable[jumpIndex] = i;
				jumpIndex++;
			}
		}
		last1CardFirstNotNull = jumpIndex - 1;
//		System.out.println("Utols� 1 k�rty�s nem null�val kezd�d�: " +jumpIndex);
		for (int i = 0; i < jumpTable1CardFirst0.length; i++) {
			jumpTable1CardFirst0[i] = jumpIndex;
			reverseJumpTable[jumpIndex] = i;
			jumpIndex++;
		}
		last1CardFirstNull = jumpIndex - 1;
//		System.out.println("Utols� 1 k�rty�s null�val kezd�d�: " +jumpIndex);
		numberOfStates = jumpIndex + 1;
//		System.out.println("NumberOfStates: " + numberOfStates);
		return jumpIndex;
	}
	private boolean isAllCharSmallerThan7(String number)
	{
		for (int j = 0; j < number.length(); j++) {
			if((int)number.charAt(j) > (int)'6')
			{
				return false;
			}
		}
		return true;
	}
	public void setRewardVectors(List<Card> deckOfA, List<Card> deckOfB)
	{
		rewardA = new double[numberOfStates];
		rewardB = new double[numberOfStates];
		for (int i = 100000; i < jumpTable3Card.length; i++) {
			if(jumpTable3Card[i] != 0)
			{
				String hash = Integer.toString(i);
				char AFirstCard = hash.charAt(0);
				char ASecondCard = hash.charAt(1);
				char AThirdCard = hash.charAt(2);
				char BFirstCard = hash.charAt(3);
				char BSecondCard = hash.charAt(4);
				char BThirdCard = hash.charAt(5);
				cardsOnTableFromA.add(deckOfA.get((int)AFirstCard - (int)'0'));
				cardsOnTableFromA.add(deckOfA.get((int)ASecondCard - (int)'0'));
				cardsOnTableFromA.add(deckOfA.get((int)AThirdCard - (int)'0'));
				cardsOnTableFromB.add(deckOfB.get((int)BFirstCard - (int)'0'));
				cardsOnTableFromB.add(deckOfB.get((int)BSecondCard - (int)'0'));
				cardsOnTableFromB.add(deckOfB.get((int)BThirdCard - (int)'0'));
				if(Player.A == showDown())
				{
					rewardA[jumpTable3Card[i]] = 10;
					rewardB[jumpTable3Card[i]] = -10;
				}
				else
				{
					rewardA[jumpTable3Card[i]] = -10;
					rewardB[jumpTable3Card[i]] = 10;
				}
			}
			cardsOnTableFromA.clear();
			cardsOnTableFromB.clear();
		}
		for (int i = 10000; i < jumpTable3CardFirst0.length; i++) {
			if(jumpTable3CardFirst0[i] != 0)
			{
				String hash = Integer.toString(i);
				char ASecondCard = hash.charAt(0);
				char AThirdCard = hash.charAt(1);
				char BFirstCard = hash.charAt(2);
				char BSecondCard = hash.charAt(3);
				char BThirdCard = hash.charAt(4);
				cardsOnTableFromA.add(deckOfA.get(0));
				cardsOnTableFromA.add(deckOfA.get((int)ASecondCard - (int)'0'));
				cardsOnTableFromA.add(deckOfA.get((int)AThirdCard - (int)'0'));
				cardsOnTableFromB.add(deckOfB.get((int)BFirstCard - (int)'0'));
				cardsOnTableFromB.add(deckOfB.get((int)BSecondCard - (int)'0'));
				cardsOnTableFromB.add(deckOfB.get((int)BThirdCard - (int)'0'));
				if(Player.A == showDown())
				{
					rewardA[jumpTable3CardFirst0[i]] = 1;
					rewardB[jumpTable3CardFirst0[i]] = -1;
				}
				else
				{
					rewardA[jumpTable3CardFirst0[i]] = -1;
					rewardB[jumpTable3CardFirst0[i]] = 1;
				}
			}
			cardsOnTableFromA.clear();
			cardsOnTableFromB.clear();
		}
//		for (int i = 0; i < i; i++) {
//			cardsOnTableFromA.add(deckOfA.get(0));
//			cardsOnTableFromA.add(deckOfA.get(0));
//			cardsOnTableFromA.add(deckOfA.get(0));
//		}
		
	}
}

